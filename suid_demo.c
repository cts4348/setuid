#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

int displayIDS();

int main(void) {

    displayIDS();

    seteuid(1001);
     
    displayIDS();

    setuid(1000);

    displayIDS();

    setuid(0); //HOW DOES THIS SUCCEED IN SETTING THE EUID BACK TO 0</b>
    
    displayIDS();
    return 0;       
}

int displayIDS(){

   printf("\n"
        "         UID           GID  \n"
        "Real      %d  Real      %d  \n"
        "Effective %d  Effective %d  \n",
             getuid (),     getgid (),
             geteuid(),     getegid()
    );
}
